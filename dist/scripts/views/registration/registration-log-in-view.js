(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['site_config', 'backbone-support', 'parse', 'jquery', 'modal', 'templates'], function(Mi, Support, Parse, $) {
    return Mi.Views.RegistrationLogInView = (function(superClass) {
      extend(RegistrationLogInView, superClass);

      function RegistrationLogInView() {
        return RegistrationLogInView.__super__.constructor.apply(this, arguments);
      }

      RegistrationLogInView.prototype.template = JST['app/scripts/templates/registration/registrationLogInTemplate'];

      RegistrationLogInView.prototype.className = 'registration-login';

      RegistrationLogInView.prototype.events = {
        'click #submit-login': 'userLogin'
      };

      RegistrationLogInView.prototype.initialize = function() {
        return console.log('Registration log in View initialized');
      };

      RegistrationLogInView.prototype.render = function() {
        this.$el.append(this.template);
        return this;
      };

      RegistrationLogInView.prototype.userLogin = function() {
        var pass, username;
        console.log('login attempted');
        username = $("#login-email").val().toLowerCase();
        pass = $("#login-password").val();
        return Parse.User.logIn(username, pass, {
          success: function(user) {
            return console.log('login successful');
          },
          error: function(user, error) {
            return console.log('login failed. error: ', error);
          }
        });
      };

      return RegistrationLogInView;

    })(Support.CompositeView);
  });

}).call(this);
