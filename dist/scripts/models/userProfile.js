(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['site_config', 'backbone', 'parse'], function(Mi, Backbone, Parse) {
    Mi.Models.UserProfile = (function(superClass) {
      extend(UserProfile, superClass);

      function UserProfile() {
        return UserProfile.__super__.constructor.apply(this, arguments);
      }

      UserProfile.prototype.initialize = function() {
        return console.log('new user profile created');
      };

      UserProfile.prototype.getProfileCompleteness = function() {
        var completeness, ref, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
        completeness = 0;
        console.log('getProfileCompleteness called, profile to check:', profileId);
        if (location) {
          completeness += 5;
        }
        if (tags.length) {
          completeness += 10;
        }
        if (profileFeatures) {
          completeness += 10;
        }
        if (profilePictureUrl) {
          completeness += 10;
        }
        if (organizations.length) {
          completeness += 5;
        }
        if (typeof skills !== "undefined" && skills !== null ? (ref = skills[0]) != null ? ref.skillHealinePhotoUrl : void 0 : void 0) {
          completeness += 20;
        }
        if (typeof skills !== "undefined" && skills !== null ? (ref1 = skills[0]) != null ? ref1.skillSummary : void 0 : void 0) {
          completeness += 20;
        }
        if (typeof skills !== "undefined" && skills !== null ? (ref2 = skills[0]) != null ? (ref3 = ref2.skillAudio) != null ? ref3.soundcloud : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        if (typeof skills !== "undefined" && skills !== null ? (ref4 = skills[0]) != null ? (ref5 = ref4.skillDocuments) != null ? ref5.length : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        if (typeof skills !== "undefined" && skills !== null ? (ref6 = skills[0]) != null ? (ref7 = ref6.skillVideo) != null ? ref7.length : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        if (typeof skills !== "undefined" && skills !== null ? (ref8 = skills[0]) != null ? (ref9 = ref8.skillPhotos) != null ? ref9.length : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        return completeness;
      };

      return UserProfile;

    })(Parse.Object);
    return this;
  });

}).call(this);
