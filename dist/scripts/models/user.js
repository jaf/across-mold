(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['site_config', 'backbone', 'parse'], function(Mi, Backbone, Parse) {
    Mi.Models.User = (function(superClass) {
      extend(User, superClass);

      function User() {
        return User.__super__.constructor.apply(this, arguments);
      }

      User.prototype.initialize = function() {
        this.profiles = new Parse.Collection;
        return console.log('new user created');
      };

      User.prototype.getProfileCompleteness = function(profile) {
        var completeness, ref, ref1, ref10, ref11, ref12, ref13, ref14, ref15, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
        completeness = 0;
        console.log('getProfileCompleteness called, profile to check:', profile);
        if (profile != null ? profile.location : void 0) {
          completeness += 5;
        }
        if (profile != null ? profile.tags.length : void 0) {
          completeness += 10;
        }
        if (profile != null ? profile.profileFeatures : void 0) {
          completeness += 10;
        }
        if (profile != null ? profile.profilePictureUrl : void 0) {
          completeness += 10;
        }
        if (profile != null ? profile.organizations.length : void 0) {
          completeness += 5;
        }
        if (profile != null ? (ref = profile.skills) != null ? (ref1 = ref[0]) != null ? ref1.skillHealinePhotoUrl : void 0 : void 0 : void 0) {
          completeness += 20;
        }
        if (profile != null ? (ref2 = profile.skills) != null ? (ref3 = ref2[0]) != null ? ref3.skillSummary : void 0 : void 0 : void 0) {
          completeness += 20;
        }
        if (profile != null ? (ref4 = profile.skills) != null ? (ref5 = ref4[0]) != null ? (ref6 = ref5.skillAudio) != null ? ref6.soundcloud : void 0 : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        if (profile != null ? (ref7 = profile.skills) != null ? (ref8 = ref7[0]) != null ? (ref9 = ref8.skillDocuments) != null ? ref9.length : void 0 : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        if (profile != null ? (ref10 = profile.skills) != null ? (ref11 = ref10[0]) != null ? (ref12 = ref11.skillVideo) != null ? ref12.length : void 0 : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        if (profile != null ? (ref13 = profile.skills) != null ? (ref14 = ref13[0]) != null ? (ref15 = ref14.skillPhotos) != null ? ref15.length : void 0 : void 0 : void 0 : void 0) {
          completeness += 5;
        }
        return completeness;
      };

      return User;

    })(Parse.User);
    return this;
  });

}).call(this);
